package com.doubledb.syncdb;

import com.doubledb.syncdb.jdbctemplate.user.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.Assert;

@SpringBootTest
class SyncDbApplicationTests {

    @Autowired
    UserService userService;

    @Test
    void contextLoads() {
        // after added 2 users to primary mariadb this is passed.
        Assert.assertEquals(2, userService.findAll().size());
    }

}
