package com.doubledb.syncdb.primary.repository;

import com.doubledb.syncdb.primary.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
