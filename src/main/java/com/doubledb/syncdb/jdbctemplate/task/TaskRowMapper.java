package com.doubledb.syncdb.jdbctemplate.task;

import com.doubledb.syncdb.primary.domain.User;
import com.doubledb.syncdb.secondary.domain.Task;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskRowMapper implements RowMapper<Task> {

    @Override
    public Task mapRow(ResultSet rs, int rowNum) throws SQLException {

        Task task = new Task();
        task.setId(rs.getLong("id"));
        task.setTaskName(rs.getString("taskname"));

        return task;
    }


}
