package com.doubledb.syncdb.jdbctemplate.task;

import com.doubledb.syncdb.primary.domain.User;
import com.doubledb.syncdb.secondary.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Task> findAll() {
        String sql = "select * from tasks";

        List<Task> tasks = jdbcTemplate.query(
                sql,
                new TaskRowMapper());

        return tasks;
    }

    public Task findByTaskID(Long id) {
        String sql = "SELECT * FROM tasks WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new TaskRowMapper(), id);
    }

}