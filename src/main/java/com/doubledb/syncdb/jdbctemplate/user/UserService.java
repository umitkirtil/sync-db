package com.doubledb.syncdb.jdbctemplate.user;

import com.doubledb.syncdb.primary.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<User> findAll() {
        String sql = "select * from users";

        List<User> customers = jdbcTemplate.query(
                sql,
                new UserRowMapper());

        return customers;
    }

    public User findByUserID(Long id) {
        String sql = "SELECT * FROM users WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new UserRowMapper(), id);
    }

}