package com.doubledb.syncdb.jdbctemplate.user;

import com.doubledb.syncdb.primary.domain.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {

        User user = new User();
        user.setId(rs.getLong("id"));
        user.setUsername(rs.getString("username"));

        return user;
    }


}
