package com.doubledb.syncdb.secondary.repository;

import com.doubledb.syncdb.secondary.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
